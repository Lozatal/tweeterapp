<?php

namespace tweeterapp\view;

class TweeterView extends \mf\view\AbstractView {

	/* Constructeur 
	*
	* Appelle le constructeur de la classe \mf\view\AbstractView
	*/
	public function __construct( $data ){
		parent::__construct($data);
	}

	/* Méthode renderHeader
	*
	*  Retourne le fragment HTML de l'entête (unique pour toutes les vues)
	*/ 
	private function renderHeader(){
		return '<h1>MiniTweeTR</h1>';
	}

	/* Méthode renderFooter
	*
	* Retourne  le fragment HTML du bas de la page (unique pour toutes les vues)
	*/
	private function renderFooter(){
		return '<p>La super app créée en Licence Pro &copy;2017</p>';
	}

	/* Méthode renderHome
	*
	* Retourne le fragment HTML qui réalise la fonctionalité afficher
	* tout les Tweets. 
	*  
	* L'attribut $this->data contient un tableau d'objets tweet.
	* 
	*/

	private function renderHome(){
		$chaine="";
		
		foreach($this->data as $ligne){
			$chaine.= $this->renderViewTweet($ligne);
		}
		
		return $chaine;
	}

	/* Méthode renderUserTweets
	*
	* Retourne le fragment HTML qui réalise la fonctionalité afficher
	* tout les Tweets d'un utilisateur donné. 
	*  
	* L'attribut $this->data contient un objet User.
	* 
	*/

	private function renderUserTweets(){

		$chaine= "<div id='userInfo'>";
		$chaine.= "<p id='fullname'>".$this->data[0]->fullname."</p><br>\n";
		$chaine.= "<p>".$this->data[0]->username."</p><br>\n";
		$chaine.= "<p>".$this->data[0]->followers." followers</p><br>\n";
		$chaine.= "</div>";

		foreach($this->data as $tweet){
			$chaine.= $this->renderViewTweet($tweet);
		}
		$chaine.="</div>";
		return $chaine;
	}

	/* Méthode renderViewTweet 
	* 
	* Retourne le fragment HTML qui réalise l'affichage d'un tweet particulié 
	* 
	* L'attribut $this->data contient un objet Tweet
	*
	*/
	private function renderViewTweet($tweet){
		$chaine='';
		$linkheader = $this->script_name."/user?id=".$tweet->author;
		$linkbody = $this->script_name."/view?id=".$tweet->id;

		$chaine.="<article class='tweet'>";
		$chaine.="<section class='tweetheader'>\n<h2><a href='".$linkheader."'>".$tweet->fullname."</a></h2>\n";
		$chaine.= "<div class='tweetheaderdate'>Publié le ".$tweet->created_at."</div>";
		$chaine.= "</section>";

		$chaine.="<section class='tweetbody'>\n<a href='".$linkbody."'>".$tweet->text."</a>\n</section>\n";
		if (isset($tweet->score)){
			$chaine.="<section class='tweetfooter'>\n<p>\n".$tweet->score."\n</p>\n</section>\n";
		}
		$chaine.= "</article>";

		return $chaine;
	}



	/* Méthode renderBody
	*
	* Retourne la framgment HTML de la balise <body> elle est appelée
	* par la méthode héritée render.
	*
	* En fonction du selecteur (un string) passé en paramètre, elle remplit les
	* blocs avec le résultats des différentes méthodes définit plus
	* haut
	* 
	*/

	protected function renderBody($selector=null){

	switch($selector){
		case 'home':{$main=$this->renderHome();break;};
		case 'userTweet':{$main=$this->renderUserTweets();break;};
		case 'viewTweet':{$main=$this->renderViewTweet($this->data);break;};
		default:{$main=$this->renderHome();break;};
	}
	$chaine="<body>";
	$chaine.="<header><a href=".$this->script_name.">".$this->renderHeader()."</a></header>";
	$chaine.="<div class='body'>".$main."</div>";
	$chaine.="<footer>".$this->renderFooter()."</footer>";
	$chaine.="</body>";


	$html = <<<EOT
	$chaine
EOT;

	return  $html;

	}

}
