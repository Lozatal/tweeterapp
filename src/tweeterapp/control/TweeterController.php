<?php

namespace tweeterapp\control;

use tweeterapp\model\Tweet as tweet;
use tweeterapp\model\User as user;



/* Classe TweeterController :
 *  
 * Réalise les algorithmes des fonctionnalités suivantes: 
 *
 *  - afficher la liste des Tweets 
 *  - afficher un Tweet
 *  - afficher les tweet d'un utilisateur 
 *  - afficher la le formulaire pour poster un Tweet
 *  - afficher la liste des utilisateurs suivis 
 *  - évaluer un Tweet
 *  - suivre un utilisateur
 *   
 */

class TweeterController extends \mf\control\AbstractController {


    /* Constructeur :
     * 
     * Appelle le constructeur parent
     *
     * c.f. la classe \mf\control\AbstractController
     * 
     */
    
    public function __construct(){
	parent::__construct();
    }


    /* Méthode viewHome : 
     * 
     * Réalise la fonctionnalité : afficher la liste de Tweet
     * 
     */
    
    public function viewHome(){

	$listTweet = user::join('tweet', 'user.id', '=', 'tweet.author')
	->select('user.fullname', 'tweet.id', 'author', 'tweet.text', 'tweet.created_at')
	->get();

	$vue = new \tweeterapp\view\TweeterView($listTweet);
	return $vue->render('home');
    }


    /* Méthode viewTweet : 
     *  
     * Réalise la fonctionnalité afficher un Tweet
     *
     */
    
    public function viewTweet(){

	$get=$this->request->get;
	$result = 0;

	if(isset($get['id'])){
		$tweet = user::select('user.fullname', 'tweet.id', 'author', 'tweet.text', 'tweet.created_at', 'tweet.score')
		->where('tweet.id', '=', $get['id'])
		->join('tweet', 'user.id', '=', 'tweet.author')
		->first();
	}

	$vue = new \tweeterapp\view\TweeterView($tweet);
	return $vue->render('viewTweet');
	//return $tweet;

	/* Algorithme : 
	 *  
	 *  1 L'identifiant du Tweet en question est passé en paramètre (id) 
	 *      d'une requête GET 
	 *  2 Récupérer le Tweet depuis le modèle Tweet
	 *  3 Afficher toutes les informations du tweet 
	 *      (text, auteur, date, score)
	 * 
	 *  Erreurs possibles : (*** à implanter ultérieurement ***)
	 *    - pas de paramètre dans la requête
	 *    - le paramètre passé ne correspond pas a un identifiant existant
	 *    - le paramètre passé n'est pas un entier 
	 * 
	 */



    }


    /* Méthode viewUserTweets :
     *
     * Réalise la fonctionnalité afficher les tweet d'un utilisateur
     *
     */
	    
    public function viewUserTweets(){

	$get=$this->request->get;
	$result = 0;

	if(isset($get['id'])){

		$listTweet = user::select('user.fullname', 'user.username', 'followers', 'tweet.id', 'author', 'tweet.text', 'tweet.created_at')
		->where('user.id', '=', $get['id'])
		->join('tweet', 'user.id', '=', 'tweet.author')
		->get();

		/*$listTweet = user::select('user.fullname', 'user.username', 'followers', 'tweet.id', 'author', 'tweet.text', 'tweet.created_at')
		->where('user.id', '=', $get['id'])
		->with('tweet')
		->get();*/

		$vue = new \tweeterapp\view\TweeterView($listTweet);	
		return $vue->render('userTweet');
	}

	/*
	 *
	 *  1 L'identifiant de l'utilisateur en question est passé en 
	 *      paramètre (id) d'une requête GET 
	 *  2 Récupérer l'utilisateur et ses Tweets depuis le modèle 
	 *      Tweet et User
	 *  3 Afficher les informations de l'utilisateur 
	 *      (non, login, nombre de suiveurs) 
	 *  4 Afficher ses Tweets (text, auteur, date)
	 * 
	 *  Erreurs possibles : (*** à implanter ultérieurement ***)
	 *    - pas de paramètre dans la requête
	 *    - le paramètre passé ne correspond pas a un identifiant existant
	 *    - le paramètre passé n'est pas un entier 
	 * 
	 */

      
    }
}
