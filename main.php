<?php

function affichageTableau($tab){
	$texte="<table><tr>";
	$int1=$tab[0];
	foreach($int1 as $cle => $valeur){
		$texte.="<th>".$cle."</th>";
	}
	$texte.="</tr>";
	foreach($tab as $cle => $tab2){
		 $texte.="<tr>";
		foreach($tab2 as $valeur){
			$texte.="<td>".utf8_encode($valeur)."</td>";
		}
		$texte.="</tr>";
	}
	$texte.="</table>";
	return $texte;
}

use tweeterapp\model as m;
use tweeterapp\model\User as user;
use tweeterapp\model\Tweet as tweet;
use mf\utils as utils;
use mf\router\Router as router;

require_once ("vendor/autoload.php");

require_once ("src/mf/utils/ClassLoader.php");
$loader = new \mf\utils\ClassLoader('src/');
$loader->register();

$config=parse_ini_file("conf/config.ini");
$db = new Illuminate\Database\Capsule\Manager();
$db->addConnection($config);
$db->setAsGlobal();
$db->bootEloquent();
/*
//Liste des utilisateurs

$listeUtilisateur=user::select()->get();
foreach ($listeUtilisateur as $user){
	echo "Identifiant = $user->id, Nom = $user->fullname \n<br/>" ;
}
echo "\n\n<br/><br/>";
//Liste des Tweets par date de modification

$listeTweet=tweet::select('text')->orderBy('updated_at')->get();
foreach ($listeTweet as $tweet){
	echo "Texte = $tweet->text \n<br/>" ;
}
echo "\n\n<br/><br/>";
//Liste des Tweets avec un score positif

$listeTweetPositif=tweet::select('text')->where('score','>','0')->orderBy('updated_at')->get();
foreach ($listeTweetPositif as $tweet){
	echo "Texte = $tweet->text \n<br/>" ;
}
echo "\n\n<br/><br/>";

//Ajout d'un tweet dans la BDD

$tweet = new tweet();
$tweet->text="Vers l'infini et au delà !";
$tweet->author=2;
//$tweet->save();
*/
//Test le controleur
$http_req = new utils\HttpRequest();

$ctrl = new tweeterapp\control\TweeterController();
//echo $ctrl->viewHome();



$router = new router();

$router->addRoute('home',    '/home/',         '\tweeterapp\control\TweeterController', 'viewHome');
$router->addRoute('view',    '/view',         '\tweeterapp\control\TweeterController', 'viewTweet');
$router->addRoute('user',    '/user',         '\tweeterapp\control\TweeterController', 'viewUserTweets');
$router->addRoute('default', 'DEFAULT_ROUTE',  '\tweeterapp\control\TweeterController', 'viewHome');
$router->run();


//print_r($ctrl->viewHome());

//$ctrl->viewHome();

